var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var session = require("express-session");
var MongoStore = require("connect-mongo")(session);
var multer = require("multer");
var morgan = require("morgan");
var jsonwebtoken = require("jsonwebtoken");
var BlackList = require("./src/db/blacklistschema");
var env = require("./src/config/env");
var auth = require("./src/middlewares/auth");
var util = require("./src/middlewares/util");
var report = require("./src/middlewares/report");
var General = require("./src/db/generalSchema");
var exec = require("child_process").execFile;
var logger = require("./src/config/logger").createLogger("error.log"); // logs to a file
var cron = require("node-cron"),
  nodemailer = require("nodemailer"),
  logger = require("./src/config/logger").createLogger("schedule.log"); // logs to a schedule file

//connect to MongoDB
mongoose.connect("mongodb://localhost/netorder");
var db = mongoose.connection;

//handle mongo error
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function() {
  // we're connected!
});

//use sessions for tracking logins
app.use(
  session({
    secret: "work hard",
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({
      mongooseConnection: db
    }),
    cookie: {
      httpOnly: false,
      secure: false
    }
  })
);

app.use(morgan("dev"));

// parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function(req, res, next) {
  if (req.headers && req.headers.authorization) {
    var token = req.headers.authorization;
    BlackList.findOne({ token: token }).exec(function(err, node) {
      if (!node) {
        jsonwebtoken.verify(token, env.default.secretKey, function(
          err,
          decode
        ) {
          if (err) {
            req.user = undefined;
          } else {
            req.user = decode;
          }
          next();
        });
      } else {
        req.user = undefined;
        next();
      }
    });
  } else {
    req.user = undefined;
    next();
  }
});

//for sending emails
let transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "ndgnetoder@gmail.com",
    pass: "NDGnetoder!123"
  }
});

// sending emails daily
cron.schedule("59 59 23 * * *", function() {
  let today = new Date();
  report.periodDayData(today, function(err, datas) {
    let content = "";
    if (datas.length == 0) {
      content = "Không có doanh thu trong ngày này.";
    } else {
      content =
        "- Ca 1 : " +
        datas[0] +
        " VND" +
        "<br/>" +
        "- Ca 2 : " +
        datas[1] +
        " VND" +
        "<br/>" +
        "- Ca 3 : " +
        datas[2] +
        " VND" +
        "<br/><br/>" +
        "Tổng doanh thu : " +
        datas[3] +
        " VND";
    }
    logger.info("Running Daily Cron Job");
    let mailOptions = {
      from: "ndgnetoder@gmail.com",
      to: "taidai1569@gmail.com",
      subject: `Thông tin doanh thu ngày : ` + util.dayFormat(today),
      text: content
    };
    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        logger.error(error);
      } else {
        logger.info("Daily email successfully sent!");
      }
    });
  });
});

// sending emails monthly
cron.schedule("0 0 0 1 * *", function() {
  let month = new Date(new Date().setDate(0));
  report.monthData(month, function(err, datas) {
    let content = "";
    if (datas.length == 0) {
      content = "Không có doanh thu trong tháng này.";
    } else {
      content =
        "Tổng doanh thu tháng " +
        (month.getMonth() + 1) +
        " như sau: " +
        datas[0] +
        " VND";
    }
    logger.info("Running Monthly Cron Job");
    let mailOptions = {
      from: "ndgnetoder@gmail.com",
      to: "nguyenvanquyen8496@gmail.com",
      subject: `Thông tin doanh thu tháng ` + util.monthFormat(month),
      text: content
    };
    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        logger.error(error);
      } else {
        logger.info("Monthly email successfully sent!");
      }
    });
  });
});

// listen on port 5000
var server = app.listen(5000, function() {
  console.log("Express app listening on port 5000");
  General.find().exec(function(err, settings) {
    if (err || settings.length == 0 || !settings[0].active) {
      exec("active.exe", function(err, data) {
        console.log(err);
        console.log(data.toString());
      });
    }
  });
});

var io = require("socket.io").listen(server);
// handle incoming connections from clients
io.sockets.on("connection", function(socket) {
  // once a client has connected, we expect to get a ping from them saying what room they want to join
  socket.on("room", function(room) {
    socket.join(room);
  });
});

// include routes
var queueRoutes = require("./src/api/queue")(io);
app.use("/queues", auth.checkLicense, queueRoutes);

var userRoutes = require("./src/api/user")();
app.use("/users", auth.checkLicense, userRoutes);

var statisticRoutes = require("./src/api/statistic")();
app.use("/statistics", auth.checkLicense, statisticRoutes);

var productRoutes = require("./src/api/product")();
app.use("/products", auth.checkLicense, productRoutes);

var pendingProductRoutes = require("./src/api/pendingproduct")();
app.use("/pendingproducts", auth.checkLicense, pendingProductRoutes);

var clientRoutes = require("./src/api/client")();
app.use("/clients", auth.checkLicense, clientRoutes);

var imageRoutes = require("./src/api/image")();
app.use("/images", auth.checkLicense, imageRoutes);

var machineRoutes = require("./src/api/machine")();
app.use("/machines", machineRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error("Unknown error");
  err.status = 404;
  next(err);
});

// error handler
// define as the last app.use callback
app.use(function(err, req, res, next) {
  console.log(err);
  logger.error(err + "\n");
  res.status(err.status || 500);
  return res.json({
    isError: true,
    message: err.message,
    result: {}
  });
});
