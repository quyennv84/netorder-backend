"use strict";

module.exports = {
  message: {
    notFindInfo: "Không thể tìm thấy thông tin.",
    requiredInfo: "Thiếu thông tin.",
    initializeError: "Lỗi khi khởi tạo.",
    notSave: "Lỗi khi lưu.",
    notUpdate: "Lỗi khi cập nhật.",
    notDelete: "Lỗi khi xóa.",
    success: "Thành công.",
    loginFailed: "Đăng nhập thất bại.",
    logoutError: "Lỗi khi đăng xuất.",
    invalidPermission: "Không có quyền thực thi.",
    invalidLicense: "Không có bản quyền.",
    resetPasswordFailed: "Khôi phục mật khẩu thất bại.",
    outOfStock: "hết hàng.",
    invalidOrder: "Đơn hàng không hợp lệ.",
    activeOk: "Kích hoạt thành công.",
  },
  default: {
    password: "12345678",
    secretKey: "RESTFULAPIs",
    MAILER_SERVICE_PROVIDER: "Gmail",
    RSA_Sceret_Key: "-----BEGIN PRIVATE KEY-----"+
    "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAP22KQfW5+TviGDO"+
    "70HwSWybk8O0Wn5S9ct6x9sXiKZoXuL9sTA+TU3WqWVACNGjdn74bfPE32ehVKqb"+
    "EeLioGm6LlwGxjtY+QhcWlUpFGD94+sPbr2CAD9STtS4wNiaADsBsTKZt9/BOHq8"+
    "pUf4I1qBUBbjZWfihIvoTHuki5CtAgMBAAECgYAWxFbwPie3EouhbFBqEV71YQyk"+
    "fjW0qksmRFFU5sq7bBg/tFZHJwAW4RF4ZYOlFE8Y28A6JMuLaB3J5Xp4aq/MP0x0"+
    "e+CeeB8+ZpzVDwN5+PVNNzhE06VbFCmcBFyKqjJr1EwR46RHd1JTuPrFOdX33sTb"+
    "azrRgoWaFlWeo8a5/QJBAP+uFjCsQ8hlcVFIjiU+odhejKLj1b3fZaB2TtmjoCi/"+
    "IPuCMSsFP/wfH+opVG1j0vSf1zGV/KRTaeCMy9fAdjMCQQD+B3FlOUACjPNga/E+"+
    "7Oe9u8YW/CscWw/d9Ixm/ZKxBaN17faxbAODudkW5FPN0cgHaVSKqpqWuiOOfKbQ"+
    "yD2fAkEApv3Y2GlqzBQVcvVkgcbqhhsa17je/WhPRAKBdXx9Op+6twpNer+vDsh1"+
    "W8cqsVeljMhpEKXZoKoqWNMcwAmgKQJBAKOvPhUgd6ttEWGfDDskwgqS1Qh4gnrp"+
    "/mZhrpqqZ/xjCrnxlNUsKXOWrFjA1XM46rBD4bLcTXI0DLdCgOblMDcCQAVI7mIV"+
    "kZE3Yf1AWxw295MFM78Z1D/EcpgHPpGmV+NmYiaOYIcN/b4B8P62fyMHHCSNpEWT"+
    "p1Alfg+VULw1cJ0="+
    "-----END PRIVATE KEY-----",
    RSA_Public_Key: " -----BEGIN PUBLIC KEY-----"+
    "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQD9tikH1ufk74hgzu9B8Elsm5PD"+
    "tFp+UvXLesfbF4imaF7i/bEwPk1N1qllQAjRo3Z++G3zxN9noVSqmxHi4qBpui5c"+
    "BsY7WPkIXFpVKRRg/ePrD269ggA/Uk7UuMDYmgA7AbEymbffwTh6vKVH+CNagVAW"+
    "42Vn4oSL6Ex7pIuQrQIDAQAB"+
    "-----END PUBLIC KEY-----"
  },
  color: {
    morning: "",
    afternoon: "",
    night: ""
  },
  room: {
    employee: "employee"
  },
  event: {
    changOrder: "Change order",
    newOrder: "New order"
  },
  secureQuestions: [
    "Tên gọi hồi nhỏ của bạn là gì ?",
    "Bộ phim yêu thích của bạn là gì ?",
    "Môn thể thao yêu thích của bạn là gì ?",
    "Việc làm đầu tiên của bạn là gì ?",
    "Đội bóng yêu thích của bạn là gì ?",
    "Món ăn yêu thích của bạn lúc nhỏ là gì ?",
    "Con của bạn sinh vào thứ mấy ?",
    "Trường tiểu học của bạn là trường nào ?"
  ]
};
