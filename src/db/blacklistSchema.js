var mongoose = require("mongoose");

var BlackListSchema = new mongoose.Schema({
  token: {
    type: String,
    required: true,
    trim: true,
    unique:true
  }
});

var BlackList = mongoose.model("BlackList", BlackListSchema);
module.exports = BlackList;
