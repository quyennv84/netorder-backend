var mongoose = require("mongoose");

var GeneralSchema = new mongoose.Schema({
  active: {
    type: Boolean,
    required: true,
  }
});

var General = mongoose.model("General", GeneralSchema);
module.exports = General;