var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");
var timestamps = require("mongoose-timestamp");

var itemSchema = new mongoose.Schema({
  itemname: {
    type: String,
    required: true
  },
  itemid: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  inittotal: {
    type: Number,
    required: true
  },
  note: {
    type: String,
    sparse: true
  },
  total: {
    type: Number,
    required: true
  },
  imageid:{
    type: String,
    required: false
  }
});

var QueueSchema = new mongoose.Schema(
  {
    sumup: {
      type: Number,
      sparse: true
    },
    items: [itemSchema],
    customername: {
      type: String,
      trim: true,
      sparse: true
    },
    customerid: {
      type: String,
      trim: true,
      required: true
    },
    status: {
      type: String,
      enum: ["NEW", "PROGRESSING", "DONE", "CANCELED"],
      default: "NEW"
    },
    sessionid: {
      type: String,
      required: true
    },
    comment: {
      type: String,
      trim: true,
      sparse: true
    },
    updateby: {
      type: String
    },
    location: {
      type: String
    },
    handlerid: {
      type: String
    },
    handlername: {
      type: String
    }
  },
  { timestamps: true },
  { versionKey: false }
);
QueueSchema.plugin(uniqueValidator);
var Queue = mongoose.model("Queue", QueueSchema);
module.exports = Queue;
