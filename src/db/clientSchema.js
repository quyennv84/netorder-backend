var mongoose = require("mongoose");
var uniqueValidator = require('mongoose-unique-validator');
var timestamps = require('mongoose-timestamp');

var ClientSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true
    },
    macaddress: {
      type: String,
      unique: true,
      required: true,
      trim: true
    },
    location: {
      type: String,
      required: true,
      trim: true,
      sparse: true
    },
    updateby: {
      type: String
    }
  },
  { versionKey: false }
);
ClientSchema.plugin(uniqueValidator);
ClientSchema.plugin(timestamps);

var Client = mongoose.model("Client", ClientSchema);
module.exports = Client;
