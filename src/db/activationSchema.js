var mongoose = require("mongoose");
var NodeRSA = require("node-rsa");
var env = require("../config/env");
var key = new NodeRSA(env.default.RSA_Sceret_Key);

var ActivationSchema = new mongoose.Schema({
  key: {
    type: String,
    required: true,
    trim: true,
    unique:true
  },
  usage:{
    type: String,
    required: true,
  }
});

ActivationSchema.pre("save", function(next) {
  var activation = this;
  var usage = key.encrypt(activation.usage, "base64");
  activation.usage = usage;
  next();
});

var Activation = mongoose.model("Activation", ActivationSchema);
module.exports = Activation;