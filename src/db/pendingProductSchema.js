var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");

var itemSchema = new mongoose.Schema({
  itemname: {
    type: String,
    required: true
  },
  itemid: {
    type: String,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  }
});

var PendingProductSchema = new mongoose.Schema(
  {
    items: [itemSchema],
    name :{
      type: String
    },
    handlerid: {
      type: String,
      trim: true
    },
    handlername: {
      type: String,
      trim: true
    },
    requesterid: {
      type: String,
      trim: true
    },
    requestername: {
      type: String,
      trim: true
    },
    status: {
      type: String,
      enum: ["NEW", "APPROVED", "REJECTED", "CANCELED"],
      default: "NEW"
    }
  },
  { timestamps: true },
  { versionKey: false }
);
PendingProductSchema.plugin(uniqueValidator);
var PendingProduct = mongoose.model("PendingProduct", PendingProductSchema);
module.exports = PendingProduct;
