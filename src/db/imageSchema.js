var mongoose = require("mongoose");

var ImageSchema = new mongoose.Schema(
  {
    img: {
      data: Buffer,
      contentType: String
    }
  },
  { versionKey: false }
);

var Image = mongoose.model("Image", ImageSchema);
module.exports = Image;
