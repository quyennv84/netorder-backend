var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");
var timestamps = require("mongoose-timestamp");

var ProductSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
      unique: true
    },
    description: {
      type: String,
      trim: true,
      sparse: true
    },
    price: {
      type: Number,
      required: true
    },
    imageid: {
      type: String
    },
    updateby: {
      type: String
    },
    instock: {
      type: Number,
      required: true,
      sparse: true,
      default: 0,
      min: 0
    },
    type: {
      type: String
    }
  },
  { versionKey: false }
);
ProductSchema.plugin(uniqueValidator);
ProductSchema.plugin(timestamps);

ProductSchema.statics.updateStock = function(req, callback) {
  var itemids = new Array();
  for (var index = 0; index < req.body.items.length; index++) {
    itemids.push(req.body.items[index].itemid);
  }
  Product.find({ _id: { $in: itemids } }, function(err, list) {
    if (err || !list.length || !list) {
      return callback(err);
    } else {
      var invalidItems = new Array();
      for (var i = 0; i < list.length; i++) {
        for (var j = 0; j < req.body.items.length; j++) {
          if (
            list[i]._id == req.body.items[j].itemid &&
            list[i].instock < req.body.items[i].quantity
          ) {
            invalidItems.push(list[i].name);
            break;
          }
        }
      }

      return callback(null, invalidItems);
    }
  });
};

var Product = mongoose.model("Product", ProductSchema);
module.exports = Product;
