var mongoose = require("mongoose");
var bcrypt = require("bcrypt");
var uniqueValidator = require("mongoose-unique-validator");
var timestamps = require("mongoose-timestamp");
var UserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      trim: true,
      sparse: true
    },
    fullname: {
      type: String,
      trim: true,
      sparse: true
    },
    address: {
      type: String,
      trim: true,
      sparse: true
    },
    phone: {
      type: String,
      trim: true,
      sparse: true
    },
    username: {
      type: String,
      required: true,
      trim: true,
      unique: true
    },
    password: {
      type: String,
      required: true
    },
    role: {
      type: String,
      enum: ["EMPLOYEE", "ADMIN", "MANAGER", "STOCKER"],
      default: "MANAGER"
    },
    updateby: {
      type: String
    },
    question: {
      type: String
    },
    answer: {
      type: String
    }
  },
  { versionKey: false }
);

//authenticate input against database
UserSchema.statics.authenticate = function(userid, password, callback) {
  User.findOne({ _id: userid }).exec(function(err, user) {
    if (err || !user) {
      return callback(err);
    }
    bcrypt.compare(password, user.password, function(err, result) {
      if (result === true) {
        return callback(null, user);
      } else {
        return callback();
      }
    });
  });
};

UserSchema.statics.authenticateByName = function(username, password, callback) {
  User.findOne({ username: username }).exec(function(err, user) {
    if (err || !user) {
      return callback(err);
    }
    bcrypt.compare(password, user.password, function(err, result) {
      if (result === true) {
        return callback(null, user);
      } else {
        return callback();
      }
    });
  });
};

UserSchema.statics.getUserList = function(req) {
  return User.count().then(count =>
    User.find().then(data => ({ total: count, rows: data }))
  );
};

//hashing a password before saving it to the database
UserSchema.pre("save", function(next) {
  var user = this;
  if (!user.isModified("password")) {
    next();
  }

  bcrypt.hash(user.password, 10, function(err, hash) {
    if (err) {
      return next(err);
    }
    user.password = hash;
    next();
  });
  user.update = Date.now;
});
UserSchema.plugin(uniqueValidator);
UserSchema.plugin(timestamps);

var User = mongoose.model("User", UserSchema);
module.exports = User;
