var User = require("../db/userschema");
var env = require("../config/env");
var Client = require("../db/clientschema");
var General = require("../db/generalSchema");
var exec = require('child_process').execFile;

module.exports = {
  requiresLogin: function(req, res, next) {
    if (req.user) {
      next();
    } else {
      var err = new Error(env.message.invalidPermission);
      err.status = 401;
      return next(err);
    }
  },

  requiresAdmin: function(req, res, next) {
    if (req.user.type === "user" && req.user.userid && req.user) {
      User.findById(req.user.userid).exec(function(err, user) {
        if (err || !user) {
          var err = new Error(env.message.invalidPermission);
          err.status = 401;
          return next(err);
        } else if (user.role == "ADMIN") {
          next();
        } else {
          var err = new Error(env.message.invalidPermission);
          err.status = 401;
          return next(err);
        }
      });
    } else {
      var err = new Error(env.message.invalidPermission);
      err.status = 401;
      return next(err);
    }
  },

  requiresAdminOrStocker: function(req, res, next) {
    if (req.user.type === "user" && req.user.userid && req.user) {
      User.findById(req.user.userid).exec(function(err, user) {
        if (err || !user) {
          var err = new Error(env.message.invalidPermission);
          err.status = 401;
          return next(err);
        } else if (user.role == "ADMIN" || user.role == "STOCKER") {
          next();
        } else {
          var err = new Error(env.message.invalidPermission);
          err.status = 401;
          return next(err);
        }
      });
    } else {
      var err = new Error(env.message.invalidPermission);
      err.status = 401;
      return next(err);
    }
  },

  requiresStocker: function(req, res, next) {
    if (req.user.type === "user" && req.user.userid && req.user) {
      User.findById(req.user.userid).exec(function(err, user) {
        if (err || !user) {
          var err = new Error(env.message.invalidPermission);
          err.status = 401;
          return next(err);
        } else if (user.role == "STOCKER") {
          next();
        } else {
          var err = new Error(env.message.invalidPermission);
          err.status = 401;
          return next(err);
        }
      });
    } else {
      var err = new Error(env.message.invalidPermission);
      err.status = 401;
      return next(err);
    }
  },

  requiresGuest: function(req, res, next) {
    if (req.user && req.user.type == "guest") {
      next();
    } else {
      var err = new Error("You must be guest.");
      err.status = 401;
      return next(err);
    }
  },

  requiresUser: function(req, res, next) {
    if (req.user && req.user.type == "user") {
      next();
    } else {
      var err = new Error("You must be authenticated user.");
      err.status = 401;
      return next(err);
    }
  },

  isExist: function(req, res, next) {
    if (req.user && req.user.type == "user") {
      User.findById(req.user.userid).exec(function(err, user) {
        if (err || !user) {
          var err = new Error(env.message.invalidPermission);
          err.status = 401;
          return next(err);
        } else {
          next();
        }
      });
    } else if (req.user && req.user.type == "guest") {
      Client.findOne({ macaddress: req.user.username }).exec(function(
        err,
        user
      ) {
        if (err || !user) {
          var err = new Error(env.message.invalidPermission);
          err.status = 401;
          return next(err);
        } else {
          next();
        }
      });
    }
  },

  //abc.exe is excutable file for entering server license key
  checkLicense: function(req, res, next) {
    General.find().exec(function(err, settings) {
      if (err || settings.length == 0 || !settings[0].active) {
        exec("active.exe", function(err, data) {
          console.log(err);
          console.log(data.toString());
        });
      } else {
        next();
      }
    });
  }
};
