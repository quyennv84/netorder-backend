var crypto = require("crypto"),
  User = require("../db/userschema"),
  Queue = require("../db/queueschema"),
  util = require("../middlewares/util");
  env = require("../config/env");
class ProductData {
  constructor(itemid, itemname, value) {
    this.itemid = itemid;
    this.value = value;
    this.itemname = itemname;
  }
  getValue() {
    return this.value;
  }
  setValue(value) {
    this.value = value;
  }
}

module.exports = {
  topProductsByRevenue: function(startdate, enddate, callback) {
    Queue.find({
      updatedAt: {
        $gte: new Date(startdate),
        $lte: new Date(enddate)
      },
      status: "DONE"
    }).exec(function(err, list) {
      var products = new Array();
      if (err || !list) {
        return callback();
      } else {
        for (var index = 0; index < list.length; index++) {
          var items = list[index].items;
          for (var iterator = 0; iterator < items.length; iterator++) {
            if (util.contains(products, items[iterator].itemid)) {
              let product = products.find(
                obj => obj.itemid == items[iterator].itemid
              );
              product.setValue(product.getValue() + items[iterator].total);
            } else {
              let product = new ProductData(
                items[iterator].itemid,
                items[iterator].itemname,
                items[iterator].total
              );
              products.push(product);
            }
          }
        }
        var topValues = products.sort((a, b) => a.value > b.value).slice(0, 5);
        return callback(null, topValues);
      }
    });
  },

  topProductsBySales: function(startdate, enddate, callback) {
    Queue.find({
      updatedAt: {
        $gte: new Date(startdate),
        $lte: new Date(enddate)
      },
      status: "DONE"
    }).exec(function(err, list) {
      var products = new Array();
      if (err || !list) {
        return callback();
      } else {
        for (var index = 0; index < list.length; index++) {
          var items = list[index].items;
          for (var iterator = 0; iterator < items.length; iterator++) {
            if (util.contains(products, items[iterator].itemid)) {
              let product = products.find(
                obj => obj.itemid == items[iterator].itemid
              );
              product.setValue(product.getValue() + items[iterator].quantity);
            } else {
              let product = new ProductData(
                items[iterator].itemid,
                items[iterator].itemname,
                items[iterator].quantity
              );
              products.push(product);
            }
          }
        }
        var topValues = products.sort((a, b) => a.value > b.value).slice(0, 5);
        return callback(null, topValues);
      }
    });
  }
};
