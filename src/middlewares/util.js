var crypto = require("crypto"),
  User = require("../db/userschema"),
  env = require("../config/env"),
   exec = require('child_process').execFile;

module.exports = {
  generate_key: function() {
    var sha = crypto.createHash("sha256");
    sha.update(Math.random().toString());
    return sha.digest("hex");
  },

  dayFormat: function(date) {
    var day = new Date(date).getDate();
    var month = new Date(date).getMonth() + 1;
    var year = new Date(date).getFullYear();
    if (day < 10) day = "0" + day;
    if (month < 10) month = "0" + month;
    return day + "/" + month + "/" + year;
  },

  monthFormat: function(date) {
    var month = new Date(date).getMonth() + 1;
    var year = new Date(date).getFullYear();
    if (month < 10) month = "0" + month;
    return month + "/" + year;
  },

  yearFormat: function(date) {
    var year = new Date(date).getFullYear();

    return year;
  },

  compareDays: function(date1, date2) {
    return (
      new Date(date1).getDate() == new Date(date2).getDate() &&
      new Date(date1).getMonth() == new Date(date2).getMonth() &&
      new Date(date1).getFullYear() == new Date(date2).getFullYear()
    );
  },

  compareMonths: function(date1, date2) {
    return (
      new Date(date1).getMonth() == new Date(date2).getMonth() &&
      new Date(date1).getFullYear() == new Date(date2).getFullYear()
    );
  },

  compareYears: function(date1, date2) {
    return new Date(date1).getFullYear() == new Date(date2).getFullYear();
  },

  getDaysArray: function(startdate, enddate) {
    var start = new Date(startdate);
    var end = new Date(enddate);
    var days = [];
    for (
      var d = new Date(start.getFullYear(), start.getMonth(), start.getDate());
      d <= new Date(end.getFullYear(), end.getMonth(), end.getDate());
      d.setDate(d.getDate() + 1)
    ) {
      days.push(new Date(d));
    }

    return days;
  },

  getMonthsArray: function(startdate, enddate) {
    var start = new Date(startdate);
    var end = new Date(enddate);
    var months = [];
    for (
      var d = new Date(start.getFullYear(), start.getMonth(), 1);
      d <= new Date(end.getFullYear(), end.getMonth(), 1);
      d.setMonth(d.getMonth() + 1)
    ) {
      months.push(new Date(d));
    }

    return months;
  },

  getYearArray: function(startdate, enddate) {
    var start = new Date(startdate);
    var end = new Date(enddate);
    var years = [];
    for (
      var d = new Date(start.getFullYear(), 0, 1);
      d <= new Date(end.getFullYear(), 0, 1);
      d.setFullYear(d.getFullYear() + 1)
    ) {
      years.push(new Date(d));
    }

    return years;
  },

  initializeUser: function(req, res, next) {
    User.getUserList(req)
      .then(data => {
        if (!data.rows.length) {
          var user = {
            username: "UcOc5Q0Cf0",
            password: "7atBqsPs04",
            role: "ADMIN"
          };

          User.create(user, function(err, firstUser) {
            if (err || !firstUser) {
              var err = new Error(env.message.initializeError);
              err.status = 200;
              next(err);
            } else {
              next();
            }
          });
        } else {
          next();
        }
      })
      .catch();
  },

  contains: function(arr, id) {
    var found = false;
    for (var i = 0; i < arr.length; i++) {
      if (arr[i].itemid == id) {
        found = true;
        break;
      }
    }
    return found;
  }
};
