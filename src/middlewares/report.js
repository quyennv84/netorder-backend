var Queue = require("../db/queueschema"),
  MORING_TIME = 6,
  AFTERNOON_TIME = 14,
  NIGHT_TIME = 22;

module.exports = {
  periodDayData: function(date, callback) {
    var startdate = new Date(new Date(date).setHours(0, 0, 0));
    var enddate = new Date(new Date(date).setHours(23, 59, 59));

    Queue.find(
      {
        updatedAt: {
          $gte: new Date(startdate),
          $lte: new Date(enddate)
        },
        status: "DONE"
      },
      function(err, list) {
        let data = new Array();
        if (err || list.length == 0) {
          return callback(null, data);
        } else {
          var morningRevenue = 0,
            afternoonRevenue = 0,
            nightRevenue = 0;
          for (var i = 0; i < list.length; i++) {
            var hour = new Date(list[i].updatedAt).getHours();
            if (hour >= MORING_TIME && hour < AFTERNOON_TIME) {
              morningRevenue += list[i].sumup;
            } else if (hour >= AFTERNOON_TIME && hour < NIGHT_TIME) {
              afternoonRevenue += list[i].sumup;
            } else {
              nightRevenue += list[i].sumup;
            }
          }

          /** 1st element is morning revenue
           *  2nd element is afternoon revenue
           *  3rd element is night revenue
           *  4th element is total revenue
           */
          data.push(morningRevenue);
          data.push(afternoonRevenue);
          data.push(nightRevenue);
          data.push(morningRevenue + afternoonRevenue + nightRevenue);
        }

        return callback(null, data);
      }
    );
  },

  monthData: function(date, callback) {
    var y = date.getFullYear(),
      m = date.getMonth();
    var firstDay = new Date(y, m, 1).setHours(0, 0, 0);
    var lastDay = new Date(y, m + 1, 0).setHours(23, 59, 59);
    var startdate = new Date(new Date(firstDay));
    var enddate = new Date(new Date(lastDay));

    Queue.find(
      {
        updatedAt: {
          $gte: new Date(startdate),
          $lte: new Date(enddate)
        },
        status: "DONE"
      },
      function(err, list) {
        let data = new Array();
        if (err || list.length == 0) {
          return callback(null, data);
        } else {
          var revenue = 0;
          for (var i = 0; i < list.length; i++) {
            revenue += list[i].sumup;
          }

          data.push(revenue);
        }
        return callback(null, data);
      }
    );
  }
};
