var cron = require("node-cron"),
  nodemailer = require("nodemailer"),
  logger = require("../config/logger").createLogger("schedule.log"); // logs to a file

let transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "ndgnetorder@gmail.com",
    pass: "ndgnetorder123"
  }
});

// sending emails at periodic intervals
cron.schedule("* * * * *", function() {
  logger.info("---------------------");
  logger.info("Running Cron Job");
  let mailOptions = {
    from: "ndgnetorder@gmail.com",
    to: "nguyenvanquyen8496@gmail.com",
    subject: `Not a GDPR update ;)`,
    text: `Hi there, this email was automatically sent by us`
  };
  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      logger.error(error);
    } else {
      logger.info("Email successfully sent!");
    }
  });
});
