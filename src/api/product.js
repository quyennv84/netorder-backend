module.exports = function() {
  var express = require("express");
  var router = express.Router();
  var Product = require("../db/productschema");
  var auth = require("../middlewares/auth");
  var env = require("../config/env");

  router.put("/product", auth.requiresAdminOrStocker, function(req, res, next) {
    if (!req.body.name || !req.body.price) {
      // cho phep instock = 0 trong truong hop stocker them san pham moi
      var err = new Error(env.message.requiredInfo);
      err.status = 200;
      next(err);
    } else {
      var product = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        instock: req.body.instock,
        imageid: req.body.imageid,
        type: req.body.type
      };

      Product.create(product, function(err, product) {
        if (!product || err) {
          var err = new Error(env.message.initializeError);
          err.status = 200;
          next(err);
        } else {
          return res.json({
            isError: false,
            message: env.message.success,
            result: product
          });
        }
      });
    }
  });

  router.post("/product", auth.requiresAdmin, function(req, res, next) {
    Product.findById(req.body.productid, function(err, product) {
      if (!product || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        product.name = req.body.name == null ? product.name : req.body.name;
        product.description =
          req.body.description == null
            ? product.description
            : req.body.description;
        product.price = req.body.price == null ? product.price : req.body.price;
        product.instock =
          req.body.instock == null ? product.instock : req.body.instock;
        product.imageid = req.body.imageid;
        product.type = req.body.type;

        product.save(function(err, updatedProduct) {
          if (err || !updatedProduct) {
            var err = new Error(env.message.notFindInfo);
            err.status = 200;
            next(err);
          } else {
            return res.json({
              isError: false,
              message: env.message.success,
              result: updatedProduct
            });
          }
        });
      }
    });
  });

  router.get("/product", auth.requiresLogin, function(req, res, next) {
    var productid = req.query.productid;
    Product.findById(productid, function(err, product) {
      if (!product || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        return res.json({
          isError: false,
          message: env.message.success,
          result: product
        });
      }
    });
  });

  router.get("/products", function(req, res, next) {
    Product.find().exec(function(err, list) {
      if (!list || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        return res.json({
          isError: false,
          message: env.message.success,
          result: list
        });
      }
    });
  });

  router.delete("/product", auth.requiresAdmin, function(req, res, next) {
    Product.deleteOne({ _id: req.query.productid }, function(err) {
      if (err) {
        var err = new Error(env.message.notDelete);
        err.status = 200;
        next(err);
      } else {
        res.json({
          isError: false,
          message: env.message.success,
          result: {}
        });
      }
    });
  });

  return router;
};
