module.exports = function() {
  var express = require("express"),
    router = express.Router(),
    Client = require("../db/clientschema"),
    auth = require("../middlewares/auth"),
    env = require("../config/env"),
    util = require("../middlewares/util"),
    User = require("../db/userschema"),
    jwt = require("jsonwebtoken");

  router.put("/client", function(req, res, next) {
    User.authenticateByName(req.body.username, req.body.password, function(
      err,
      user
    ) {
      if (err || !user || user.role == "STOCKER" || user.role == "MANAGER") {
        var err = new Error(env.message.invalidPermission);
        err.status = 200;
        next(err);
      } else {
        if (!req.body.name || !req.body.macaddress || !req.body.location) {
          var err = new Error(env.message.requiredInfo);
          err.status = 200;
          next(err);
        } else {
          var clientData = {
            name: req.body.name,
            macaddress: req.body.macaddress,
            location: req.body.location
          };
          Client.create(clientData, function(err, client) {
            if (!client || err) {
              var err = new Error(env.message.initializeError);
              err.status = 200;
              next(err);
            } else {
              return res.json({
                isError: false,
                message: env.message.success,
                result: client
              });
            }
          });
        }
      }
    });
  });

  router.put("/client-admin", auth.requiresAdmin, function(req, res, next) {
    if (!req.body.name || !req.body.macaddress || !req.body.location) {
      var err = new Error(env.message.requiredInfo);
      err.status = 200;
      next(err);
    } else {
      var clientData = {
        name: req.body.name,
        macaddress: req.body.macaddress,
        location: req.body.location
      };
      Client.create(clientData, function(err, client) {
        if (!client || err) {
          var err = new Error(env.message.initializeError);
          err.status = 200;
          next(err);
        } else {
          return res.json({
            isError: false,
            message: env.message.success,
            result: client
          });
        }
      });
    }
  });

  router.post("/client", auth.requiresAdmin, function(req, res, next) {
    Client.findById(req.body.clientid, function(err, client) {
      if (!client || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        client.name = req.body.name == null ? client.name : req.body.name;
        client.macaddress =
          req.body.macaddress == null ? client.macaddress : req.body.macaddress;
        client.location =
          req.body.location == null ? client.location : req.body.location;

        client.save(function(err, updatedClient) {
          if (err || !updatedClient) {
            var err = new Error(env.message.notUpdate);
            err.status = 200;
            next(err);
          } else {
            return res.json({
              isError: false,
              message: env.message.success,
              result: updatedClient
            });
          }
        });
      }
    });
  });

  router.get("/client", auth.requiresAdmin, function(req, res, next) {
    var clientid = req.query.clientid;
    Client.findById(clientid, function(err, client) {
      if (!client || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        return res.json({
          isError: false,
          message: env.message.success,
          result: client
        });
      }
    });
  });

  router.get("/clients", auth.requiresUser, function(req, res, next) {
    Client.find().exec(function(err, results) {
      if (!results || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        return res.json({
          isError: false,
          message: env.message.success,
          result: results
        });
      }
    });
  });

  router.delete("/client", auth.requiresAdmin, function(req, res, next) {
    Client.deleteOne({ _id: req.query.clientid }, function(err) {
      if (err) {
        var err = new Error(env.message.notDelete);
        err.status = 200;
        next(err);
      } else {
        res.json({
          isError: false,
          message: env.message.success,
          result: {}
        });
      }
    });
  });

  router.get("/auto-client", function(req, res, next) {
    Client.findOne({ macaddress: req.query.macaddress }).exec(function(
      err,
      client
    ) {
      if (!client || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        var sessionid = util.generate_key();
        var token = jwt.sign(
          { type: "guest", userid: sessionid, username: client.macaddress },
          env.default.secretKey
        );
        return res.json({
          isError: false,
          message: env.message.success,
          result: {
            sessionid: sessionid,
            token: token,
            info: client
          }
        });
      }
    });
  });

  return router;
};
