module.exports = function() {
  var express = require("express");
  var router = express.Router();
  var Activation = require("../db/activationschema");
  var env = require("../config/env");
  var NodeRSA = require("node-rsa");
  var mongoose = require("mongoose");
  var General = mongoose.model("General");
  var key = new NodeRSA(env.default.RSA_Sceret_Key);

  router.post("/active", function(req, res, next) {
    if (!req.body.key) {
      var err = new Error(env.message.requiredInfo);
      err.status = 200;
      return next(err);
    } else {
      Activation.findOne({ key: req.body.key }, function(err, activation) {
        if (err || !activation) {
          var err = new Error(env.message.invalidLicense);
          err.status = 200;
          return next(err);
        } else {
          var usage = key.decrypt(activation.usage, "utf8");
          var usageLeft = Number(usage);
          if (usageLeft == 0) {
            var err = new Error(env.message.invalidLicense);
            err.status = 200;
            return next(err);
          } else {
            usageLeft--;
            activation.usage = usageLeft.toString();
            activation.save(function(err, updated) {
              if (err || !updated) {
                var err = new Error(env.message.invalidLicense);
                err.status = 200;
                return next(err);
              } else {
                General.findOneAndUpdate({}, { $set: { active: true } }).exec(
                  function(err, updated) {
                    if (err || !updated) {
                      var err = new Error(env.message.invalidLicense);
                      err.status = 200;
                      return next(err);
                    } else {
                      return res.json({
                        isError: false,
                        message: env.message.success,
                        result: {
                          isactivated: true
                        }
                      });
                    }
                  }
                );
              }
            });
          }
        }
      });
    }
  });

  return router;
};
