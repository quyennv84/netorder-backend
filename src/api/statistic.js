module.exports = function() {
  var express = require("express"),
    router = express.Router(),
    Queue = require("../db/queueschema"),
    auth = require("../middlewares/auth"),
    env = require("../config/env"),
    util = require("../middlewares/util"),
    calculation = require("../middlewares/calculation"),
    env = require("../config/env"),
    MORING_TIME = 6,
    AFTERNOON_TIME = 14,
    NIGHT_TIME = 22;

  class GroupChartPoint {
    constructor(title, color, styleId, childs) {
      this.title = title;
      this.color = color;
      this.styleId = styleId;
      this.chartPoints = childs;
    }
  }

  class ChartPoint {
    constructor(date, value) {
      this.date = date;
      this.value = value;
      this.viewvalue = "";
    }

    getValue() {
      return this.value;
    }

    setValue(value) {
      this.value = value;
    }

    setViewValue(viewvalue) {
      this.viewvalue = viewvalue;
    }

    setDate(date) {
      this.date = date;
    }

    getDate() {
      return this.date;
    }
  }

  function getPeriodDaysData(listDays, data) {
    var period = new Array();
    for (var i = 0; i < listDays.length; i++) {
      var child = new ChartPoint(listDays[i], 0);
      period.push(child);
    }
    for (var i = 0; i < period.length; i++) {
      for (var j = 0; j < data.length; j++) {
        if (util.compareDays(period[i].getDate(), data[j].updatedAt)) {
          period[i].setValue(period[i].getValue() + data[j].sumup);
        }
      }
      period[i].setViewValue(period[i].getValue().toString());
      period[i].setDate(util.dayFormat(period[i].getDate()));
    }

    return period;
  }

  function getPeriodMonthData(listMonths, data) {
    var period = new Array();
    for (var i = 0; i < listMonths.length; i++) {
      var child = new ChartPoint(listMonths[i], 0);
      period.push(child);
    }
    for (var i = 0; i < period.length; i++) {
      for (var j = 0; j < data.length; j++) {
        if (util.compareMonths(period[i].getDate(), data[j].updatedAt)) {
          period[i].setValue(period[i].getValue() + data[j].sumup);
        }
      }
      period[i].setViewValue(period[i].getValue().toString());
      period[i].setDate(util.monthFormat(period[i].getDate()));
    }

    return period;
  }

  function getPeriodYearData(listYears, data) {
    var period = new Array();
    for (var i = 0; i < listYears.length; i++) {
      var child = new ChartPoint(listYears[i], 0);
      period.push(child);
    }
    for (var i = 0; i < period.length; i++) {
      for (var j = 0; j < data.length; j++) {
        if (util.compareYears(period[i].getDate(), data[j].updatedAt)) {
          period[i].setValue(period[i].getValue() + data[j].sumup);
        }
      }
      period[i].setViewValue(period[i].getValue().toString());
      period[i].setDate(util.yearFormat(period[i].getDate()));
    }

    return period;
  }

  function clone(a) {
    return JSON.parse(JSON.stringify(a));
  }

  router.post("/days", function(req, res, next) {
    var startdate = req.body.startdate;
    var enddate = req.body.enddate;
    Queue.find({
      updatedAt: {
        $gte: new Date(startdate),
        $lte: new Date(enddate)
      },
      status: "DONE"
    }).exec(function(err, list) {
      if (!list || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        var listDays = util.getDaysArray(startdate, enddate);

        var days = getPeriodDaysData(listDays, list);

        var dayColumn = new GroupChartPoint(
          "Ngày",
          env.color.morning,
          "",
          days
        );

        return res.json({
          isError: false,
          message: env.message.success,
          result: {
            chartName: "Biểu đồ doanh thu theo ngày",
            // xAxisUnit: "Ngày",
            yAxisUnit: "VND",
            groupChartPoints: [dayColumn]
          }
        });
      }
    });
  });

  router.post("/days-period", function(req, res, next) {
    var startdate = req.body.startdate;
    var enddate = req.body.enddate;
    Queue.find({
      updatedAt: {
        $gte: new Date(startdate),
        $lte: new Date(enddate)
      },
      status: "DONE"
    }).exec(function(err, list) {
      if (!list || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        var morningData = new Array();
        var afternoonData = new Array();
        var nightData = new Array();
        for (var i = 0; i < list.length; i++) {
          var hour = new Date(list[i]).getHours();
          if (hour >= MORING_TIME && hour < AFTERNOON_TIME) {
            morningData.push(list[i]);
          } else if (hour >= AFTERNOON_TIME && hour < NIGHT_TIME) {
            afternoonData.push(list[i]);
          } else {
            nightData.push(list[i]);
          }
        }

        var listDays = util.getDaysArray(startdate, enddate);

        var mornings = getPeriodDaysData(listDays, morningData);

        var afternoons = getPeriodDaysData(listDays, afternoonData);

        var nights = getPeriodDaysData(listDays, nightData);

        var morningColumn = new GroupChartPoint(
          "Sáng",
          env.color.morning,
          "",
          mornings
        );
        var afternoonColumn = new GroupChartPoint(
          "Chiều",
          env.color.afternoon,
          "",
          afternoons
        );
        var nightColumn = new GroupChartPoint(
          "Tối",
          env.color.night,
          "",
          nights
        );

        var columns = new Array();
        columns.push(morningColumn);
        columns.push(afternoonColumn);
        columns.push(nightColumn);

        return res.json({
          isError: false,
          message: env.message.success,
          result: {
            chartName: "Biểu đồ doanh thu các ngày theo ca",
            // xAxisUnit: "Ngày",
            yAxisUnit: "VND",
            groupChartPoints: columns
          }
        });
      }
    });
  });

  router.post("/months-period", function(req, res, next) {
    var startdate = new Date(req.body.startdate);
    var enddate = new Date(req.body.enddate);
    enddate = new Date(
      new Date(enddate.getFullYear() + 1, 0, 0).setHours(23, 59, 59)
    );
    Queue.find({
      updatedAt: {
        $gte: startdate,
        $lte: enddate
      },
      status: "DONE"
    }).exec(function(err, list) {
      if (!list || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        var morningData = new Array();
        var afternoonData = new Array();
        var nightData = new Array();
        for (var i = 0; i < list.length; i++) {
          var hour = new Date(list[i]).getHours();
          if (hour >= MORING_TIME && hour < AFTERNOON_TIME) {
            morningData.push(list[i]);
          } else if (hour >= AFTERNOON_TIME && hour < NIGHT_TIME) {
            afternoonData.push(list[i]);
          } else {
            nightData.push(list[i]);
          }
        }

        var listMonths = util.getMonthsArray(startdate, enddate);

        var mornings = getPeriodMonthData(listMonths, morningData);

        var afternoons = getPeriodMonthData(listMonths, afternoonData);

        var nights = getPeriodMonthData(listMonths, nightData);

        var morningColumn = new GroupChartPoint(
          "Sáng",
          env.color.morning,
          "",
          mornings
        );
        var afternoonColumn = new GroupChartPoint(
          "Chiều",
          env.color.afternoon,
          "",
          afternoons
        );
        var nightColumn = new GroupChartPoint(
          "Tối",
          env.color.night,
          "",
          nights
        );

        var columns = new Array();
        columns.push(morningColumn);
        columns.push(afternoonColumn);
        columns.push(nightColumn);

        return res.json({
          isError: false,
          message: env.message.success,
          result: {
            chartName: "Biểu đồ doanh thu các tháng theo ca",
            // xAxisUnit: "Tháng",
            yAxisUnit: "VND",
            groupChartPoints: columns
          }
        });
      }
    });
  });

  router.post("/months", function(req, res, next) {
    var startdate = new Date(req.body.startdate);
    var enddate = new Date(req.body.enddate);
    enddate = new Date(
      new Date(enddate.getFullYear() + 1, 0, 0).setHours(23, 59, 59)
    );

    Queue.find({
      updatedAt: {
        $gte: startdate,
        $lte: enddate
      },
      status: "DONE"
    }).exec(function(err, list) {
      if (!list || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        var listMonths = util.getMonthsArray(startdate, enddate);
        var months = getPeriodMonthData(listMonths, list);
        var monthColumn = new GroupChartPoint("Tháng", "", "", months);

        return res.json({
          isError: false,
          message: env.message.success,
          result: {
            chartName: "Biểu đồ doanh thu theo tháng",
            // xAxisUnit: "Tháng",
            yAxisUnit: "VND",
            groupChartPoints: [monthColumn]
          }
        });
      }
    });
  });

  router.post("/years-period", function(req, res, next) {
    var startdate = new Date(req.body.startdate);
    var enddate = new Date(req.body.enddate);
    enddate = new Date(
      new Date(enddate.getFullYear() + 1, 0, 0).setHours(23, 59, 59)
    );

    Queue.find({
      updatedAt: {
        $gte: startdate,
        $lte: enddate
      },
      status: "DONE"
    }).exec(function(err, list) {
      if (!list || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        var morningData = new Array();
        var afternoonData = new Array();
        var nightData = new Array();
        for (var i = 0; i < list.length; i++) {
          var hour = new Date(list[i]).getHours();
          if (hour >= MORING_TIME && hour < AFTERNOON_TIME) {
            morningData.push(list[i]);
          } else if (hour >= AFTERNOON_TIME && hour < NIGHT_TIME) {
            afternoonData.push(list[i]);
          } else {
            nightData.push(list[i]);
          }
        }

        var listYears = util.getYearArray(startdate, enddate);

        var mornings = getPeriodYearData(listYears, morningData);

        var afternoons = getPeriodYearData(listYears, afternoonData);

        var nights = getPeriodYearData(listYears, nightData);

        var morningColumn = new GroupChartPoint(
          "Sáng",
          env.color.morning,
          "",
          mornings
        );
        var afternoonColumn = new GroupChartPoint(
          "Chiều",
          env.color.afternoon,
          "",
          afternoons
        );
        var nightColumn = new GroupChartPoint(
          "Tối",
          env.color.night,
          "",
          nights
        );

        var columns = new Array();
        columns.push(morningColumn);
        columns.push(afternoonColumn);
        columns.push(nightColumn);

        return res.json({
          isError: false,
          message: env.message.success,
          result: {
            chartName: "Biểu đồ doanh thu các năm theo ca",
            // xAxisUnit: "Năm",
            yAxisUnit: "VND",
            groupChartPoints: columns
          }
        });
      }
    });
  });

  router.post("/years", function(req, res, next) {
    var startdate = new Date(req.body.startdate);
    var enddate = new Date(req.body.enddate);
    enddate = new Date(
      new Date(enddate.getFullYear() + 1, 0, 0).setHours(23, 59, 59)
    );
    Queue.find({
      updatedAt: {
        $gte: startdate,
        $lte: enddate
      },
      status: "DONE"
    }).exec(function(err, list) {
      if (!list || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        var listYears = util.getYearArray(startdate, enddate);

        var years = getPeriodYearData(listYears, list);

        var yearColumn = new GroupChartPoint("Năm", "", "", years);

        return res.json({
          isError: false,
          message: env.message.success,
          result: {
            chartName: "Biểu đồ doanh thu các năm",
            // xAxisUnit: "Năm",
            yAxisUnit: "VND",
            groupChartPoints: [yearColumn]
          }
        });
      }
    });
  });

  router.post("/line/revenue", function(req, res, next) {
    var startdate = new Date(req.body.startdate);
    var enddate = new Date(req.body.enddate);
    var category = req.body.category;
    if (category == "YEAR") {
      enddate = new Date(
        new Date(enddate.getFullYear() + 1, 0, 0).setHours(23, 59, 59)
      );
    } else if (category == "MONTH") {
      enddate = new Date(
        new Date(enddate.getFullYear(), enddate.getMonth() + 1, 0).setHours(
          23,
          59,
          59
        )
      );
    }
    Queue.find({
      updatedAt: {
        $gte: new Date(startdate),
        $lte: new Date(enddate)
      },
      status: "DONE"
    }).exec(function(err, list) {
      if (!list || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        calculation.topProductsByRevenue(startdate, enddate, function(
          err,
          data
        ) {
          if (err || !data || data.length == 0) {
            var err = new Error(env.message.notFindInfo);
            err.status = 200;
            next(err);
          } else {
            var lines = new Array();
            var listTimes;
            if (category == "DAY") {
              listTimes = util.getDaysArray(startdate, enddate);
              period = "ngày";
            } else if (category == "MONTH") {
              listTimes = util.getMonthsArray(startdate, enddate);
              period = "tháng";
            } else {
              listTimes = util.getYearArray(startdate, enddate);
              period = "năm";
            }

            for (var i = 0; i < data.length; i++) {
              let tempQueues = clone(list);
              for (var index = 0; index < tempQueues.length; index++) {
                let filtered = tempQueues[index].items.filter(function(item) {
                  return item.itemid == data[i].itemid;
                });
                tempQueues[index].items = filtered;
                tempQueues[index].sumup = 0;
                for (var j = 0; j < tempQueues[index].items.length; j++) {
                  tempQueues[index].sumup += tempQueues[index].items[j].total;
                }
              }
              var chartPoints;
              var period;
              if (category == "DAY") {
                chartPoints = getPeriodDaysData(listTimes, tempQueues);
              } else if (category == "MONTH") {
                chartPoints = getPeriodMonthData(listTimes, tempQueues);
              } else {
                chartPoints = getPeriodYearData(listTimes, tempQueues);
              }

              var groupChartPoint = new GroupChartPoint(
                data[i].itemname,
                "",
                "",
                chartPoints
              );
              lines.push(groupChartPoint);
            }

            return res.json({
              isError: false,
              message: env.message.success,
              result: {
                chartName:
                  "Biểu đồ 5 mặt hàng có doanh thu cao nhất qua các " + period,
                // xAxisUnit: period.charAt(0).toUpperCase(),
                yAxisUnit: "VND",
                groupChartPoints: lines
              }
            });
          }
        });
      }
    });
  });

  router.post("/line/sales", function(req, res, next) {
    var startdate = new Date(req.body.startdate);
    var enddate = new Date(req.body.enddate);
    var category = req.body.category;
    if (category == "YEAR") {
      enddate = new Date(
        new Date(enddate.getFullYear() + 1, 0, 0).setHours(23, 59, 59)
      );
    } else if (category == "MONTH") {
      enddate = new Date(
        new Date(enddate.getFullYear(), enddate.getMonth() + 1, 0).setHours(
          23,
          59,
          59
        )
      );
    }
    Queue.find({
      updatedAt: {
        $gte: new Date(startdate),
        $lte: new Date(enddate)
      },
      status: "DONE"
    }).exec(function(err, list) {
      if (!list || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        calculation.topProductsBySales(startdate, enddate, function(err, data) {
          if (err || !data || data.length == 0) {
            var err = new Error(env.message.notFindInfo);
            err.status = 200;
            next(err);
          } else {
            var lines = new Array();
            var listTimes;
            var period;
            if (category == "DAY") {
              listTimes = util.getDaysArray(startdate, enddate);
              period = "ngày";
            } else if (category == "MONTH") {
              listTimes = util.getMonthsArray(startdate, enddate);
              period = "tháng";
            } else {
              listTimes = util.getYearArray(startdate, enddate);
              period = "năm";
            }

            for (var i = 0; i < data.length; i++) {
              var tempQueues = clone(list);
              for (var index = 0; index < tempQueues.length; index++) {
                var filtered = tempQueues[index].items.filter(function(item) {
                  return item.itemid == data[i].itemid;
                });
                tempQueues[index].items = filtered;
                tempQueues[index].sumup = 0;
                for (var j = 0; j < tempQueues[index].items.length; j++) {
                  tempQueues[index].sumup +=
                    tempQueues[index].items[j].quantity;
                }
              }
              var chartPoints;
              if (category == "DAY") {
                chartPoints = getPeriodDaysData(listTimes, tempQueues);
              } else if (category == "MONTH") {
                chartPoints = getPeriodMonthData(listTimes, tempQueues);
              } else {
                chartPoints = getPeriodYearData(listTimes, tempQueues);
              }

              var groupChartPoint = new GroupChartPoint(
                data[i].itemname,
                "",
                "",
                chartPoints
              );
              lines.push(groupChartPoint);
            }

            return res.json({
              isError: false,
              message: env.message.success,
              result: {
                chartName: "Biểu đồ 5 mặt hàng bán chạy nhất qua các " + period,
                xAxisUnit: null,
                // xAxisUnit: period.charAt(0).toUpperCase(),
                yAxisUnit: "Số lượng",
                groupChartPoints: lines
              }
            });
          }
        });
      }
    });
  });

  return router;
};
