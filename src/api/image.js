module.exports = function() {
  var express = require("express");
  var router = express.Router();
  var Image = require("../db/imageschema");
  var env = require("../config/env");
  var fs = require("fs");
  var multer = require("multer");

  var storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, "uploads/");
    },
    filename: function(req, file, cb) {
      cb(null, file.originalname);
    }
  });

  var upload = multer({
    storage: storage
  });

  router.post("/image", upload.single("productImage"), function(
    req,
    res,
    next
  ) {
    var imgItem = new Image();
    imgItem.img.data = fs.readFileSync(req.file.path);
    imgItem.img.contentType = req.file.mimetype;

    Image.create(imgItem, function(err, image) {
      if (err || !image) {
        var err = new Error(env.message.initializeError);
        err.status = 200;
        next(err);
      } else {
        return res.json({
          isError: false,
          message: env.message.success,
          result: {
            _id: image._id
          }
        });
      }
    });
  });

  router.get("/image", function(req, res, next) {
    Image.findOne({ _id: req.query.imageid }, function(err, image) {
      if (err || !image) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        res.contentType(image.img.contentType);
        res.send(image.img.data);
      }
    });
  });

  return router;
};
