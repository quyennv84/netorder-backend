module.exports = function() {
  var express = require("express"),
    router = express.Router(),
    User = require("../db/userschema"),
    env = require("../config/env"),
    util = require("../middlewares/util"),
    bcrypt = require("bcrypt"),
    auth = require("../middlewares/auth"),
    jwt = require("jsonwebtoken"),
    BlackList = require("../db/blacklistschema");

  //POST route for updating data
  router.put("/user", auth.requiresAdmin, function(req, res, next) {
    if (req.body.username) {
      var userData = {
        email: req.body.email,
        username: req.body.username,
        role: req.body.role,
        phone: req.body.phone,
        address: req.body.phone,
        fullname: req.body.fullname,
        password: env.default.password
      };

      User.create(userData, function(error, user) {
        if (error || !user) {
          var err = new Error(env.message.initializeError);
          err.status = 200;
          next(err);
        } else {
          return res.json({
            isError: false,
            message: env.message.success,
            result: {
              _id: user._id,
              username: user.username,
              role: user.role,
              email: user.email,
              phone: user.phone,
              address: user.address,
              fullname: user.fullname,
              password: env.default.password
            }
          });
        }
      });
    } else {
      var err = new Error(env.message.requiredInfo);
      err.status = 200;
      next(err);
    }
  });

  router.post("/change-password", auth.requiresUser, function(req, res, next) {
    User.authenticate(req.body.userid, req.body.oldpassword, function(
      err,
      user
    ) {
      if (err || !user) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        user.password = req.body.newpassword;
        user.save(function(err, updatedUser) {
          if (err || !updatedUser) {
            var err = new Error(env.message.notUpdate);
            err.status = 200;
            next(err);
          } else {
            return res.json({
              isError: false,
              message: env.message.success,
              result: {}
            });
          }
        });
      }
    });
  });

  router.post("/user", auth.requiresUser, function(req, res, next) {
    if (req.body.userid == req.user.userid) {
      User.authenticate(req.body.userid, req.body.password, function(
        err,
        user
      ) {
        if (err || !user) {
          var err = new Error(env.message.notFindInfo);
          err.status = 200;
          next(err);
        } else {
          user.username =
            req.body.username == null ? user.username : req.body.username;
          user.email = req.body.email == null ? user.email : req.body.email;
          user.role = req.body.role == null ? user.role : req.body.role;
          user.phone = req.body.phone == null ? user.phone : req.body.phone;
          user.address =
            req.body.address == null ? user.address : req.body.address;
          user.fullname =
            req.body.fullname == null ? user.fullname : req.body.fullname;
          user.password = req.body.password;

          user.save(function(err, updatedUser) {
            if (err || !updatedUser) {
              var err = new Error(env.message.notUpdate);
              err.status = 200;
              next(err);
            } else {
              return res.json({
                isError: false,
                message: env.message.success,
                result: {
                  _id: user._id,
                  username: user.username,
                  role: user.role,
                  email: user.email,
                  address: user.address,
                  phone: user.phone,
                  fullname: user.fullname
                }
              });
            }
          });
        }
      });
    } else {
      User.authenticate(req.user.userid, req.body.password, function(
        err,
        data
      ) {
        if (err || !data || data.role != "ADMIN") {
          var err = new Error(env.message.invalidPermission);
          err.status = 200;
          return next(err);
        } else {
          User.findById(req.body.userid, function(err, user) {
            if (err || !data) {
              var err = new Error(env.message.notFindInfo);
              err.status = 200;
              next(err);
            } else {
              user.username =
                req.body.username == null ? user.username : req.body.username;
              user.email = req.body.email == null ? user.email : req.body.email;
              user.role = req.body.role == null ? user.role : req.body.role;
              user.phone = req.body.phone == null ? user.phone : req.body.phone;
              user.address =
                req.body.address == null ? user.address : req.body.address;
              user.fullname =
                req.body.fullname == null ? user.fullname : req.body.fullname;

              user.save(function(err, updatedUser) {
                if (err || !updatedUser) {
                  var err = new Error(env.message.notUpdate);
                  err.status = 200;
                  next(err);
                } else {
                  return res.json({
                    isError: false,
                    message: env.message.success,
                    result: {
                      _id: user._id,
                      username: user.username,
                      role: user.role,
                      email: user.email,
                      address: user.address,
                      phone: user.phone,
                      fullname: user.fullname
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });

  // GET route
  router.get("/user", auth.requiresLogin, function(req, res, next) {
    User.findById(req.query.userid)
      .select([
        "username",
        "role",
        "email",
        "_id",
        "address",
        "phone",
        "fullname"
      ])
      .exec(function(error, user) {
        if (error || !user) {
          var err = new Error(env.message.notFindInfo);
          err.status = 200;
          next(err);
        } else {
          return res.json({
            isError: false,
            message: env.message.success,
            result: user
          });
        }
      });
  });

  router.get("/users", auth.requiresAdmin, function(req, res, next) {
    User.find()
      .select([
        "username",
        "role",
        "email",
        "_id",
        "phone",
        "address",
        "fullname"
      ])
      .exec(function(err, results) {
        if (!results || err) {
          var err = new Error(env.message.notFindInfo);
          err.status = 200;
          next(err);
        } else {
          var userWithoutAdmin = new Array();
          for (var i = 0; i < results.length; i++) {
            if (
              results[i]._id != req.user.userid &&
              results[i].username != "UcOc5Q0Cf0"
            ) {
              userWithoutAdmin.push(results[i]);
            }
          }
          return res.json({
            isError: false,
            message: env.message.success,
            result: userWithoutAdmin
          });
        }
      });
  });

  // GET for logout
  router.get("/logout", function(req, res, next) {
    if (req.user) {
      // delete session object
      var blacknode = {
        token: req.headers.authorization
      };
      BlackList.create(blacknode, function(err, node) {
        if (err || !node) {
          var err = new Error(env.message.logoutError);
          err.status = 200;
          next(err);
        } else {
          req.user = undefined;
          return res.json({
            isError: false,
            message: env.message.success,
            result: {}
          });
        }
      });
    }
  });

  router.get("/secure-question", function(req, res, next) {
    return res.json({
      isError: false,
      message: env.message.success,
      result: {
        questions: env.secureQuestions
      }
    });
  });

  router.post("/update-secure", function(req, res, next) {
    if (!req.body.question || !req.body.answer) {
      var err = new Error(env.message.requiredInfo);
      err.status = 200;
      next(err);
    } else {
      User.findOne({ username: req.body.username }).exec(function(err, user) {
        if (err || !user) {
          var err = new Error(env.message.notFindInfo);
          err.status = 200;
          next(err);
        } else {
          user.question = req.body.question;
          bcrypt.hash(req.body.answer, 10, function(err, hash) {
            if (err) {
              var err = new Error(env.message.notUpdate);
              err.status = 200;
              next(err);
            }
            user.answer = hash;
            user.save(function(err, updatedUser) {
              if (err || !updatedUser) {
                var err = new Error(env.message.notUpdate);
                err.status = 200;
                next(err);
              } else {
                return res.json({
                  isError: false,
                  message: env.message.success,
                  result: {}
                });
              }
            });
          });
        }
      });
    }
  });

  router.post("/reset-password", auth.requiresAdmin, function(req, res, next) {
    if (!req.body.userid) {
      var err = new Error(env.message.requiredInfo);
      err.status = 200;
      next(err);
    } else {
      User.findById(req.body.userid).exec(function(err, user) {
        if (err || !user) {
          var err = new Error(env.message.notFindInfo);
          err.status = 200;
          next(err);
        } else if (user.role != "ADMIN") {
          user.password = env.default.password;
          user.save(function(err, updatedUser) {
            if (err || !updatedUser) {
              var err = new Error(env.message.resetPasswordFailed);
              err.status = 200;
              next(err);
            } else {
              return res.json({
                isError: false,
                message: env.message.success,
                result: {
                  password: env.default.password
                }
              });
            }
          });
        }
      });
    }
  });

  router.post("/forgot-password", function(req, res, next) {
    if (!req.body.username) {
      var err = new Error(env.message.requiredInfo);
      err.status = 200;
      next(err);
    } else {
      User.findOne({ username: req.body.username }).exec(function(err, user) {
        if (err || !user || user.role != "ADMIN") {
          var err = new Error(env.message.invalidPermission);
          err.status = 200;
          next(err);
        } else {
          if (user.question == req.body.question) {
            bcrypt.compare(req.body.answer, user.answer, function(err, check) {
              if (check == true) {
                user.password = env.default.password;
                user.save(function(err, updatedUser) {
                  if (err || !updatedUser) {
                    var err = new Error(env.message.resetPasswordFailed);
                    err.status = 200;
                    next(err);
                  } else {
                    return res.json({
                      isError: false,
                      message: env.message.success,
                      result: {
                        password: env.default.password
                      }
                    });
                  }
                });
              } else {
                var err = new Error(env.message.resetPasswordFailed);
                err.status = 200;
                next(err);
              }
            });
          } else {
            var err = new Error(env.message.resetPasswordFailed);
            err.status = 200;
            next(err);
          }
        }
      });
    }
  });

  router.post("/login", util.initializeUser, function(req, res, next) {
    User.findOne({ username: req.body.username }).exec(function(err, user) {
      if (err || !user) {
        var err = new Error(env.message.loginFailed);
        err.status = 200;
        next(err);
      } else {
        bcrypt.compare(req.body.password, user.password, function(err, check) {
          if (check === true) {
            req.session.type = "user";
            var sessionid = util.generate_key();
            var token = jwt.sign(
              { type: "user", userid: user._id, username: user.username },
              env.default.secretKey
            );

            var haveSecureQuestion = false;
            if (user.question) {
              haveSecureQuestion = true;
            }

            return res.json({
              isError: false,
              message: env.message.success,
              result: {
                sessionid: sessionid,
                token: token,
                info: {
                  username: user.username,
                  role: user.role,
                  phone: user.phone,
                  email: user.email,
                  address: user.address,
                  _id: user._id,
                  fullname: user.fullname
                },
                haveSecureQuestion: haveSecureQuestion
              }
            });
          } else {
            var err = new Error(env.message.loginFailed);
            err.status = 200;
            next(err);
          }
        });
      }
    });
  });

  router.delete("/user", auth.requiresAdmin, function(req, res, next) {
    User.deleteOne({ _id: req.query.userid }, function(err) {
      if (err) {
        var err = new Error(env.message.notDelete);
        err.status = 200;
        next(err);
      } else {
        res.json({
          isError: false,
          message: env.message.success,
          result: {}
        });
      }
    });
  });

  return router;
};
