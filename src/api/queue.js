module.exports = function(io) {
  var express = require("express");
  var router = express.Router();
  var Queue = require("../db/queueschema");
  var auth = require("../middlewares/auth");
  var env = require("../config/env");
  var Product = require("../db/productschema");
  var async = require("async");

  router.put("/queue", auth.requiresLogin, auth.isExist, function(
    req,
    res,
    next
  ) {
    if (
      !req.body.location ||
      !req.body.customerid ||
      !req.body.customername ||
      (typeof req.body.items === "undefined" && req.body.items.length == 0)
    ) {
      var err = new Error(env.message.requiredInfo);
      err.status = 200;
      next(err);
    } else {
      var status = "NEW";
      var handlerid, handlername;
      if (req.user.type == "user") {
        status = req.body.status;
        handlerid = req.user.userid;
        handlername = req.user.username;
      }

      var queue = {
        items: req.body.items,
        customerid: req.body.customerid,
        customername: req.body.customername,
        sumup: req.body.sumup,
        sessionid: req.body.sessionid,
        comment: req.body.comment,
        location: req.body.location,
        status: status,
        handlerid: handlerid,
        handlername: handlername
      };
      Product.updateStock(req, function(err, invalidItems) {
        if (err) {
          var err = new Error(env.message.invalidOrder);
          err.status = 200;
          next(err);
        } else {
          if (invalidItems.length == 0) {
            Queue.create(queue, function(err, queue) {
              if (!queue || err) {
                var err = new Error(env.message.initializeError);
                err.status = 200;
                next(err);
              } else {
                async.eachSeries(
                  queue.items,
                  function updateObject(obj, done) {
                    // Model.update(condition, doc, callback)
                    Product.update(
                      { _id: obj.itemid },
                      { $inc: { instock: 0 - obj.quantity } },
                      done
                    );
                  },
                  function allDone(err) {
                    // this will be called when all the updates are done or an error occurred during the iteration
                    if (err) {
                      next(err);
                    } else {
                      io.sockets
                        .in(env.room.employee)
                        .emit(env.event.newOrder, queue);
                      return res.json({
                        isError: false,
                        message: env.message.success,
                        result: queue
                      });
                    }
                  }
                );
              }
            });
          } else {
            var message = "";
            for (var i = 0; i < invalidItems.length; i++) {
              message += invalidItems[i]+" ";
              if (i != invalidItems.length - 1) {
                message += ",";
              }
            }
            var err = new Error(message +" đã hết hàng!");
            err.status = 200;
            next(err);
          }
        }
      });
    }
  });

  router.post("/queue", function(req, res, next) {
    Queue.findById(req.body.queueid, function(err, queue) {
      if (!queue || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        var oldStatus = queue.status;
        queue.items = req.body.items == null ? queue.items : req.body.items;
        if (req.user && req.user.type == "user") {
          queue.status = req.body.status;
        } else if (
          req.user &&
          req.user.type == "guest" &&
          req.body.status &&
          req.body.status == "CANCELED" &&
          queue.status == "NEW"
        ) {
          queue.status = req.body.status;
        }
        var sumup = 0;
        for (var i = 0; i < queue.items.length; i++) {
          sumup += queue.items[i].total;
        }
        queue.sumup = sumup;
        queue.comment =
          req.body.comment == null ? queue.comment : req.body.comment;
        queue.handlerid = req.body.handlerid;
        queue.handlername = req.body.handlername;
        queue.save(function(err, updatedQueue) {
          if (err || !updatedQueue) {
            var err = new Error(env.message.notUpdate);
            err.status = 200;
            next(err);
          } else {
            if (oldStatus != updatedQueue.status && oldStatus == "NEW") {
              io.sockets
                .in(env.room.employee)
                .emit(env.event.changOrder, updatedQueue);
            }
            io.emit(queue.sessionid, queue);
            if (updatedQueue.status == "CANCELED") {
              var iterator;
              for (iterator in updatedQueue.items) {
                Product.findOneAndUpdate(
                  { _id: iterator.itemid },
                  { $inc: { instock: iterator.quantity } },
                  { new: true },
                  function(err, product) {
                    if (err || !product) {
                      // var err = new Error(env.message.notUpdate);
                      // err.status = 200;
                      // next(err);
                    }
                  }
                );
              }
            }
            return res.json({
              isError: false,
              message: env.message.success,
              result: updatedQueue
            });
          }
        });
      }
    });
  });

  router.get("/queue", auth.requiresLogin, function(req, res, next) {
    var queueid = req.query.queueid;
    Queue.findById(queueid, function(err, queue) {
      if (!queue || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        return res.json({
          isError: true,
          message: env.message.success,
          result: queue
        });
      }
    });
  });

  router.get("/queues", function(req, res, next) {
    if (req.user.type == "guest") {
      Queue.find({ sessionid: req.user.userid }).exec(function(err, list) {
        if (!list || err) {
          var err = new Error(env.message.notFindInfo);
          err.status = 200;
          next(err);
        } else {
          return res.json({
            isError: false,
            message: env.message.success,
            result: list
          });
        }
      });
    } else {
      if (!req.query.filter) {
        Queue.find().exec(function(err, list) {
          if (!list || err) {
            var err = new Error(env.message.notFindInfo);
            err.status = 200;
            next(err);
          } else {
            return res.json({
              isError: false,
              message: env.message.success,
              result: list
            });
          }
        });
      } else {
        var query = req.query.handlerid
          ? { status: req.query.filter, handlerid: req.query.handlerid }
          : { status: req.query.filter };
        Queue.find(query).exec(function(err, list) {
          if (!list || err) {
            var err = new Error(env.message.notFindInfo);
            err.status = 200;
            next(err);
          } else {
            return res.json({
              isError: false,
              message: env.message.success,
              result: list
            });
          }
        });
      }
    }
  });

  router.delete("/queue", auth.requiresAdmin, function(req, res, next) {
    Queue.deleteOne({ _id: req.query.queueid }, function(err) {
      if (err) {
        var err = new Error(env.message.notDelete);
        err.status = 200;
        next(err);
      } else {
        res.json({
          isError: false,
          message: env.message.success,
          result: {}
        });
      }
    });
  });

  return router;
};
