module.exports = function() {
  var express = require("express");
  var router = express.Router();
  var Product = require("../db/productschema");
  var PendingProduct = require("../db/pendingProductschema");
  var auth = require("../middlewares/auth");
  var env = require("../config/env");
  var async = require("async");

  router.put("/request", auth.requiresStocker, function(req, res, next) {
    if (typeof req.body.items === "undefined" || req.body.items.length == 0) {
      var err = new Error(env.message.requiredInfo);
      err.status = 200;
      next(err);
    } else {
      var pendingProduct = {
        name: req.body.name,
        items: req.body.items,
        requesterid: req.user.userid,
        requestername: req.user.username
      };

      PendingProduct.create(pendingProduct, function(err, pendingProduct) {
        if (!pendingProduct || err) {
          var err = new Error(env.message.initializeError);
          err.status = 200;
          next(err);
        } else {
          return res.json({
            isError: false,
            message: env.message.success,
            result: pendingProduct
          });
        }
      });
    }
  });

  router.post("/request", auth.requiresStocker, function(req, res, next) {
    PendingProduct.findById(req.body.pendingproductid, function(
      err,
      pendingProduct
    ) {
      if (!pendingProduct || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else if (pendingProduct.requesterid != req.user.userid) {
        var err = new Error(env.message.invalidPermission);
        err.status = 200;
        next(err);
      } else {
        pendingProduct.items =
          typeof req.body.items === "undefined" || req.body.items.length == 0
            ? pendingProduct.items
            : req.body.items;

        pendingProduct.name = req.body.name;
        pendingProduct.save(function(err, updatedPendingProduct) {
          if (err || !updatedPendingProduct) {
            var err = new Error(env.message.notUpdate);
            err.status = 200;
            next(err);
          } else {
            return res.json({
              isError: false,
              message: env.message.success,
              result: updatedPendingProduct
            });
          }
        });
      }
    });
  });

  router.post("/request/decision", auth.requiresAdmin, function(
    req,
    res,
    next
  ) {
    if (
      !req.body.decision ||
      (req.body.decision != "APPROVED" && req.body.decision != "REJECTED")
    ) {
      var err = new Error(env.message.invalidPermission);
      err.status = 200;
      next(err);
    } else {
      PendingProduct.findById(req.body.pendingproductid, function(
        err,
        pendingProduct
      ) {
        if (!pendingProduct || err) {
          var err = new Error(env.message.notFindInfo);
          err.status = 200;
          next(err);
        } else if (pendingProduct.status != "NEW") {
          var err = new Error(env.message.invalidPermission);
          err.status = 200;
          next(err);
        } else {
          pendingProduct.status = req.body.decision;
          pendingProduct.handlerid = req.user.userid;
          pendingProduct.handlername = req.user.username;

          if (req.body.decision == "APPROVED") {
            async.eachSeries(
              pendingProduct.items,
              function updateObject(obj, done) {
                // Model.update(condition, doc, callback)
                Product.update(
                  { _id: obj.itemid },
                  { $inc: { instock: obj.quantity } },
                  done
                );
              },
              function allDone(err) {
                // this will be called when all the updates are done or an error occurred during the iteration
                if (err) {
                  next(err);
                } else {
                  pendingProduct.save(function(err, updatedPendingProduct) {
                    if (err || !updatedPendingProduct) {
                      var err = new Error(env.message.notUpdate);
                      err.status = 200;
                      next(err);
                    } else {
                      return res.json({
                        isError: false,
                        message: env.message.success,
                        result: updatedPendingProduct
                      });
                    }
                  });
                }
              }
            );
          } else {
            pendingProduct.save(function(err, updatedPendingProduct) {
              if (err || !updatedPendingProduct) {
                var err = new Error(env.message.notUpdate);
                err.status = 200;
                next(err);
              } else {
                return res.json({
                  isError: false,
                  message: env.message.success,
                  result: updatedPendingProduct
                });
              }
            });
          }
        }
      });
    }
  });

  router.get("/request", auth.requiresUser, function(req, res, next) {
    PendingProduct.findById(req.query.pendingproductid, function(
      err,
      pendingProduct
    ) {
      if (!pendingProduct || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        return res.json({
          isError: false,
          message: env.message.success,
          result: pendingProduct
        });
      }
    });
  });

  router.get("/requests", auth.requiresUser, function(req, res, next) {
    var filterQuery = req.query.filter ? { status: req.query.filter } : {};
    var requesterQuery = req.query.requesterid
      ? { requesterid: req.query.requesterid }
      : {};
    var query = { $and: [filterQuery, requesterQuery] };
    PendingProduct.find(query).exec(function(err, list) {
      if (!list || err) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        return res.json({
          isError: false,
          message: env.message.success,
          result: list
        });
      }
    });
  });

  router.delete("/request", auth.requiresStocker, function(req, res, next) {
    PendingProduct.findById(req.query.pendingproductid, function(
      err,
      pendingProduct
    ) {
      if (err || !pendingProduct) {
        var err = new Error(env.message.notFindInfo);
        err.status = 200;
        next(err);
      } else {
        if (req.user.userid == pendingProduct.requesterid) {
          pendingProduct.status = "CANCELED";
          pendingProduct.save(function(err, updatedPendingProduct) {
            if (err || !updatedPendingProduct) {
              var err = new Error(env.message.notUpdate);
              err.status = 200;
              next(err);
            } else {
              return res.json({
                isError: false,
                message: env.message.success,
                result: updatedPendingProduct
              });
            }
          });
        } else {
          var err = new Error(env.message.invalidPermission);
          err.status = 200;
          next(err);
        }
      }
    });
  });

  return router;
};
